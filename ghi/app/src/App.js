import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        <LocationForm />
        {/* <ConferenceForm /> */}
        {/* <AttendConferenceForm /> */}
        {/* <PresentationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>

    </>
  );
}

export default App;


  //LocationForm is a form to add locations created as a react component
// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <>
//     <Nav />
//     <div className="container">
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//         {props.attendees.map(attendee => {
//           return (
//             <tr key={attendee.href}>
//               <td>{ attendee.name }</td>
//              <td>{ attendee.conference }</td>
//           </tr>
//           );
//         })}
//         </tbody>
//       </table>
//     </div>
//     </>
//   );

// }
