import React from 'react'; // importing react libraries
import ReactDOM from 'react-dom/client';
import './index.css'; // just a normal css file, a global stylesheet
import App from './App'; // imports the app function from the App.js file app component is top-most component
// app is ultimate parent component
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root')); // connects to html div id
root.render( // tells React where to take the 'fake' html and put it
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
// structure for a basic react component that returns html
// <App /> runs the App function and JSX in the other file --how the html is built for the page
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

async function loadAttendees() {
const response = await fetch('http://localhost:8001/api/attendees/'); // url to show attendee list
if (response.ok) { // check if the response is okay
  const data = await response.json(); // if okay, get data from the response json method
  console.log(data); // print data to console
  root.render(
    <React.StrictMode>
      <App attendees={data.attendees} />
    </React.StrictMode>
  );
} else {
  console.error(response); // if not okay, print response as error
}
}
loadAttendees();




// Module parse failed: The top-level-await experiment is not enabled (set experiments.topLevelAwait: true to enabled it)
// means you can't use the await keyword at top level of javascript
