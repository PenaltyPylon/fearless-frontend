// a form to add locations as a React component
// creating a stateful component
// stateful means that it can have internal data saved in the component when it gets created, just like model classNamees in Django

import React, { useEffect, useState } from 'react';

// useState react function creates variables and functions to update the component states (hold the
// data that the user has entered in the form)
function LocationForm(props) { // a function to add locations
  const [states, setStates] = useState([]) // useState hook to store name in the component's state with
 // a default initial value of an empty string
  const [name, setName] = useState(''); // returns variable and a function
  const [roomCount, setRoomCount] = useState('');
  const [cityChange, setCityChange] = useState('');
  const [stateChange, setStateChange] = useState('');

// each of these correspond to form fields

// handle functions are responsible for updating data as soon as user enters it in
  const handleSubmit = async (event) => {
    event.preventDefault();

    // create an empty JSON object
    const data = {}; // create empty data object

    data.room_count = roomCount; // assign state values to the key names that the back end server is expecting
    data.name = name;
    data.city = cityChange;
    data.state = stateChange;

    console.log(data);


  const locationUrl = 'http://localhost:8000/api/locations/';
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);

    setName('');
    setRoomCount('');
    setCityChange('');
    setStateChange('');

  }
}
// handleSubmit collects all the data from variables and sends it to api
  const handleNameChange = (event) => { // takes what the user inputs into the form and stores it in
    // the state's name variable
    const value = event.target.value; // the event is the event that occurred, target property is the html tag that
    // caused the event (user's input and form for the location name), event.target.value property is the text
    // that the user typed into the form
    setName(value); // hmm how does it know we're using the state name variable?
  }

  const handleCountChange = (event) => {
    const value = event.target.value;
    setRoomCount(value);
  }

  const handleCityChange = (event) => {
    const value = event.target.value
    setCityChange(value);
 }

  const handleStateChange = (event) => {
    const value = event.target.value
    setStateChange(value);
 }

 // console.log(states, setStates)

 // collecting data for drop down
  async function fetchData() {
    const url = 'http://localhost:8000/api/states/';


    const response = await fetch(url); // the fetch response from the url

    if (response.ok) { // if the fetch is successful

      const data = await response.json(); // the response as json
    setStates(data.states);
// load the states data/options

    }
  }

      // const selectTag = document.getElementById('state'); // works to get info from html based on id
      // for (let state of data.states) {  // for loop to go through list of states
      //   const option = document.createElement('option'); // need to look up
      //   option.value = state.abbreviation; // the value of the option is the state abbreviation
      //   option.innerHTML = state.name; // but what's shown is the state name
      //   selectTag.appendChild(option); // still not sure how this is working
  useEffect(() => { // use this to make fetch requests, useEffect lets you sychronize a component with an external system
     fetchData();
  }, []);
  return(
  <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new location</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                  <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}></input>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" value={roomCount}></input>
                    <label htmlFor="room_count">Room count</label>
                  </div>
                  <div className="form-floating mb-3">
                <input onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" value={cityChange}></input>
                  <label htmlFor="city">City</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={handleStateChange} required name="state" id="state" className="form-select" value={stateChange}>
                      <option value="">Choose a state</option>
                      {states.map(state => { // puts the state data into the drop down, calling on states parameter
                        return (
                          <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
          </div>
      </div>
      );

                    }


export default LocationForm;
