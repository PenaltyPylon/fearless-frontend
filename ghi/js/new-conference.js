

window.addEventListener('DOMContentLoaded', async () => {
    // "Let's declare a variable that will hold the URL for the API that we just created."
    const url = 'http://localhost:8000/api/locations/';
    console.log("url", url);
    // "Let's fetch the URL. Don't forget the await keyword
    // so that we get the response, not the Promise."
     const response = await fetch(url);
     // "If the response is okay, then let's get the data
     // using the .json method. Don't forget to await that, too."
     if (response.ok) {
         const data = await response.json();
         console.log(data);
         const selectTag = document.getElementById('location'); // need help understanding this
         for (let location of data.locations) {
           // Create an 'option' element
           const option = document.createElement('option')
           option.value = location.id; // The content of this attribute represents
           // the value to be submitted with the form, should this option be selected
           option.innerHTML = location.name; // location . name or id???
           // Set the '.value' property of the option element to the
           // state's abbreviation
           // Set the '.innerHTML' property of the option element to
           // the state's name
           selectTag.appendChild(option); // need  to check mdn
         //   for (const category of data.categories.slice(0, 100)) {
         //     const option = document.createElement('option');
         //     option.innerHTML = category.title;
         //     option.value = category.id;
         //     categorySelect.appendChild(option);
           // Append the option element as a child of the select tag
         }
       }
       const formTag = document.getElementById('create-location-form');
       formTag.addEventListener('submit', async event => {
         event.preventDefault();
         console.log('need to submit the form data');
         const formData = new FormData(formTag); // create a formData object from the form element
         const json = JSON.stringify(Object.fromEntries(formData)); // turn the object into json (like json dumps?)
         console.log(json);

         const conferenceUrl = 'http://localhost:8000/api/conferences/';
         const fetchConfig = {
           method: "post",
           body: json,
           headers: {
             'Content-Type': 'application/json',
           }
         }
         const response = await fetch(conferenceUrl, fetchConfig);
         if (response.ok) {
           formTag.reset();
           const newConference = await response.json();
           console.log(newConference);
         }
       });
     });
