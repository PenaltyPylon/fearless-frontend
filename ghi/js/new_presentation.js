window.addEventListener('DOMContentLoaded', async () => {



    const url = 'http://localhost:8000/api/conferences/';
     const response = await fetch(url);


     if (response.ok) {

         const data = await response.json();
         const selectTag = document.getElementById("conference");
         const tester = document.querySelector("#conference");
         console.log("tester",tester)
         console.log("selectTag",selectTag)

         for (let conference of data.conferences) {

           const option = document.createElement('option')
           option.value = conference.href;

           console.log("conference.href",conference.href)
           option.innerHTML = conference.name;
           console.log("option.innerHTML:",option.innerHTML)
           selectTag.appendChild(option);
         }

       const formTag = document.getElementById('create-presentation-form');
       formTag.addEventListener('submit', async event => {
         event.preventDefault();
         console.log('need to submit the form data');
         const formData = new FormData(formTag);
         const json = JSON.stringify(Object.fromEntries(formData));
         console.log(json);
         const conferenceId = selectTag.value;
         const presentationUrl = `http://localhost:8000/${conferenceId}presentations/`;
         const fetchConfig = {
           method: "post",
           body: json,
           headers: {
             'Content-Type': 'application/json',
           }

         }

         const response = await fetch(presentationUrl, fetchConfig);
         if (response.ok) {
           formTag.reset();
           const newPresentation = await response.json();
           console.log(presentationUrl);
           console.log(newPresentation);

         }

       });
      }
     });
