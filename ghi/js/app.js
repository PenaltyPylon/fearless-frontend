// function createCard(name, description, pictureUrl) {
//     return `
//       <div class="card">
//         <img src="${pictureUrl}" class="card-img-top">
//         <div class="card-body">
//           <h5 class="card-title">${name}</h5>
//           <p class="card-text">${description}</p>
//         </div>
//       </div>
//     `;
//   }

//   window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();


//         for (let conference of data.conferences) {
//           const detailUrl = `http://localhost:8000${conference.href}`;
//           const detailResponse = await fetch(detailUrl);
//           if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             const title = details.conference.name;
//             const description = details.conference.description;
//             const pictureUrl = details.conference.location.picture_url;
//             const html = createCard(title, description, pictureUrl);
//             console.log(html);
//           }
//         }

//       }
//     } catch (e) {
//         console.error(e);
//       // Figure out what to do if an error is raised
//     }

//   });


  // STOP HERE

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/'; // create a variable that stores this url

//     try {
//       const response = await fetch(url); // create a variable that stores a fetch method?
//       // basically just going to the website that we stored in our url variable and requesting some data

//       if (!response.ok) { // if the response token does not = 200, meaning it's a bad response
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json(); // create a variable data which stores the json squiggly data

        // const conference = data.conferences[0]; // create a variable named conference which pulls the
        // // conference info at position 1
        // const nameTag = document.querySelector('.card-title'); // not sure how this is working,
        // // but it uses CSS to I guess transform the data with that tag
        // nameTag.innerHTML = conference.name; // change the inner html on the page
        // // to the conference name by using the conference variable which pulled the json
        // // information from the first conference -- the conference response has an
        // // attribute of "name"

        // const detailUrl = `http://localhost:8000${conference.href}`; // name a new
        // // url that points to the detail view for a conference
        // const detailResponse = await fetch(detailUrl); // create a variable that stores
        // // the fetch request to the url named in detail url
        // if (detailResponse.ok) { // if the fetch works
        //   const details = await detailResponse.json(); // create variable named details
        //   // that stores the json information from the show conference detail request
        // console.log("details", details); // this helps show what the console.log is referring to

        // const descTag = document.querySelector('.card-text'); // incorporate this stuff where
        // // this CSS tag is used
        // descTag.innerHTML = details.conference.description; // change the stuff in the inner html to the conference
        // // description details by calling details variable plus conference variable (which one?),
        // // plus description attribute from json response
        // const picture = details.conference.location.picture_url;
        // console.log("picture", picture);
        // const imageTag = document.querySelector('.card-img-top');
        // imageTag.src = details.conference.location.picture_url;
        // console.log(details);
//         }
//     }
//   }  catch (e) {
//         console.error(e);
//       // Figure out what to do if an error is raised
//     }

//   });

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();

//         for (let conference of data.conferences) {
//           const detailUrl = `http://localhost:8000${conference.href}`;
//           const detailResponse = await fetch(detailUrl);
//           if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             const title = details.conference.name;
//             const description = details.conference.description;
//             const pictureUrl = details.conference.location.picture_url;
//             const html = createCard(title, location, description, pictureUrl);
//             console.log(html);
//           }
//         }

//       }
//     } catch (e) {
//         console.error(e);
//       // Figure out what to do if an error is raised
//     }

//   });
function createCard(name, location, description, pictureUrl, starts, ends) {
    return `
      <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          ${new Date(starts).toLocaleDateString()} -
          ${new Date(ends).toLocaleDateString()}
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        let index = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const html = createCard(title, location, description, pictureUrl, starts, ends);
            const column = document.querySelector(`#col-${index % 3}`);
            column.innerHTML += html;
            index += 1;
          }
        }

      }
    } catch (e) {
      console.error(e);
      // Figure out what to do if an error is raised
    }

  });
